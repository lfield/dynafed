# - Find python libraries
# This module finds if Python is installed and determines where the
# include files and libraries are. It also determines what the name of
# the library is. This code sets the following variables:
#
#  PYTHONLIBS2_FOUND           - have the Python libs been found
#  PYTHON2_LIBRARIES           - path to the python library
#  PYTHON2_INCLUDE_PATH        - path to where Python.h is found (deprecated)
#  PYTHON2_INCLUDE_DIRS        - path to where Python.h is found
#  PYTHON2_DEBUG_LIBRARIES     - path to the debug library
#  Python2_ADDITIONAL_VERSIONS - list of additional Python versions to search for
#  PYTHON2_VERSION             - the found version (F.Furano 07062011)
#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

INCLUDE(CMakeFindFrameworks)
# Search for the python framework on Apple.
CMAKE_FIND_FRAMEWORKS(Python)


# Set up the versions we know about, in the order we will search. Always add
# the user supplied additional versions to the front.
set(_Python2_VERSIONS
  ${Python2_ADDITIONAL_VERSIONS}
  2.7 2.6 2.5 2.4 2.3 2.2 2.1 2.0)

  
FOREACH(_CURRENT_VERSION ${_Python2_VERSIONS})
  STRING(REPLACE "." "" _CURRENT_VERSION_NO_DOTS ${_CURRENT_VERSION})
  MESSAGE(STATUS "Checking Python2 version ${_CURRENT_VERSION} ${_CURRENT_VERSION_NO_DOTS}")
  IF(WIN32)
    FIND_LIBRARY(PYTHON2_DEBUG_LIBRARY
      NAMES python${_CURRENT_VERSION_NO_DOTS}_d python
      PATHS
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/libs/Debug
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/libs )
  ENDIF(WIN32)

  FIND_LIBRARY(PYTHON2_LIBRARY
    NAMES python${_CURRENT_VERSION_NO_DOTS} python${_CURRENT_VERSION}
    PATHS
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/libs
    # Avoid finding the .dll in the PATH.  We want the .lib.
    NO_SYSTEM_ENVIRONMENT_PATH
  )
  # Look for the static library in the Python config directory
  FIND_LIBRARY(PYTHON2_LIBRARY
    NAMES python${_CURRENT_VERSION_NO_DOTS} python${_CURRENT_VERSION}
    # Avoid finding the .dll in the PATH.  We want the .lib.
    NO_SYSTEM_ENVIRONMENT_PATH
    # This is where the static library is usually located
    PATH_SUFFIXES python${_CURRENT_VERSION}/config
  )

  # For backward compatibility, honour value of PYTHON2_INCLUDE_PATH, if
  # PYTHON2_INCLUDE_DIR is not set.
  IF(DEFINED PYTHON2_INCLUDE_PATH AND NOT DEFINED PYTHON2_INCLUDE_DIR)
    SET(PYTHON2_INCLUDE_DIR "${PYTHON2_INCLUDE_PATH}" CACHE PATH
      "Path to where Python.h is found for Python2" FORCE)
  ENDIF(DEFINED PYTHON2_INCLUDE_PATH AND NOT DEFINED PYTHON2_INCLUDE_DIR)

  SET(PYTHON2_FRAMEWORK_INCLUDES)
  IF(Python_FRAMEWORKS AND NOT PYTHON2_INCLUDE_DIR)
    FOREACH(dir ${Python_FRAMEWORKS})
      SET(PYTHON2_FRAMEWORK_INCLUDES ${PYTHON_FRAMEWORK_INCLUDES} ${PYTHON2_FRAMEWORK_INCLUDES}
        ${dir}/Versions/${_CURRENT_VERSION}/include/python${_CURRENT_VERSION})
    ENDFOREACH(dir)
  ENDIF(Python_FRAMEWORKS AND NOT PYTHON2_INCLUDE_DIR)

  FIND_PATH(PYTHON2_INCLUDE_DIR
    NAMES Python.h
    PATHS
      ${PYTHON_FRAMEWORK_INCLUDES}
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/include
    PATH_SUFFIXES
      /python${_CURRENT_VERSION}
  )
  MESSAGE(STATUS "Checked Python2 includes ${_CURRENT_VERSION} ${PYTHON2_INCLUDE_DIR} ${PYTHON2_FRAMEWORK_INCLUDES}")
  # For backward compatibility, set PYTHON_INCLUDE_PATH, but make it internal.
  SET(PYTHON2_INCLUDE_PATH "${PYTHON2_INCLUDE_DIR}" CACHE INTERNAL
    "Path to where Python.h is found for Python2 (deprecated)")

ENDFOREACH(_CURRENT_VERSION)


# STRING(REGEX REPLACE ".*/libpython([0-9 .]+).so" "\\1" PythonLibs_VERSION ${PYTHON_LIBRARY})
STRING(REGEX REPLACE ".*/libpython([0-9.]+[a-z]?)\\.so" "\\1" PythonLibs2_VERSION ${PYTHON2_LIBRARY})
MESSAGE(STATUS "Found PythonLibs version ${PythonLibs2_VERSION}")
MESSAGE(STATUS "Python includes are in  ${PYTHON2_INCLUDE_DIR}")


#
# Version checking
#
if(PythonLibs2_FIND_VERSION AND PythonLibs2_VERSION)
    if(PythonLibs2_FIND_VERSION_EXACT)
        if(NOT PythonLibs2_VERSION VERSION_EQUAL ${PythonLibs2_FIND_VERSION})
          MESSAGE(STATUS "Found PythonLibs2 ${PythonLibs2_VERSION} != ${PythonLibs2_FIND_VERSION} (Exact match required)")
          unset(PYTHONLIBS2_FOUND CACHE)
          RETURN()
        endif()
    else()
        # version is too low
        if(NOT PythonLibs2_VERSION VERSION_EQUAL ${PythonLibs2_FIND_VERSION} AND 
                NOT PythonLibs2_VERSION VERSION_GREATER ${PythonLibs2_FIND_VERSION})
          MESSAGE(STATUS "Found PythonLibs2 ${PythonLibs2_VERSION} < ${PythonLibs2_FIND_VERSION} (Minimum required).")
          unset(PYTHONLIBS2_FOUND CACHE)
          RETURN()    
        endif()
    endif()
endif()









MARK_AS_ADVANCED(
  PYTHON2_DEBUG_LIBRARY
  PYTHON2_LIBRARY
  PYTHON2_INCLUDE_DIR
)

# We use PYTHON_INCLUDE_DIR, PYTHON_LIBRARY and PYTHON_DEBUG_LIBRARY for the
# cache entries because they are meant to specify the location of a single
# library. We now set the variables listed by the documentation for this
# module.
SET(PYTHON2_INCLUDE_DIRS "${PYTHON2_INCLUDE_DIR}")
SET(PYTHON2_LIBRARIES "${PYTHON2_LIBRARY}")
SET(PYTHON2_DEBUG_LIBRARIES "${PYTHON2_DEBUG_LIBRARY}")

#INCLUDE(${CMAKE_CURRENT_LIST_DIR}/FindPackageHandleStandardArgs.cmake)
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PythonLibs2 DEFAULT_MSG PYTHON2_LIBRARIES PYTHON2_INCLUDE_DIRS)

