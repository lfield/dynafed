# - Find python libraries
# This module finds if Python is installed and determines where the
# include files and libraries are. It also determines what the name of
# the library is. This code sets the following variables:
#
#  PYTHONLIBS_FOUND           - have the Python libs been found
#  PYTHON3_LIBRARIES           - path to the python library
#  PYTHON3_INCLUDE_PATH        - path to where Python.h is found (deprecated)
#  PYTHON3_INCLUDE_DIRS        - path to where Python.h is found
#  PYTHON3_DEBUG_LIBRARIES     - path to the debug library
#  Python3_ADDITIONAL_VERSIONS - list of additional Python versions to search for
#  PYTHON3_VERSION             - the found version (F.Furano 07062011)
#=============================================================================
# Copyright 2001-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

INCLUDE(CMakeFindFrameworks)
# Search for the python framework on Apple.
CMAKE_FIND_FRAMEWORKS(Python)


# Set up the versions we know about, in the order we will search. Always add
# the user supplied additional versions to the front.
set(_Python3_VERSIONS
  ${Python3_ADDITIONAL_VERSIONS}
  3.6m 3.6 3.5 3.4 3.3 3.2 3.1 3.0 )

  
FOREACH(_CURRENT_VERSION ${_Python3_VERSIONS})
  STRING(REPLACE "." "" _CURRENT_VERSION_NO_DOTS ${_CURRENT_VERSION})
  MESSAGE(STATUS "Checking Python3 version ${_CURRENT_VERSION} ${_CURRENT_VERSION_NO_DOTS}")
  IF(WIN32)
    FIND_LIBRARY(PYTHON3_DEBUG_LIBRARY
      NAMES python${_CURRENT_VERSION_NO_DOTS}_d python
      PATHS
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/libs/Debug
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/libs )
  ENDIF(WIN32)

  FIND_LIBRARY(PYTHON3_LIBRARY
    NAMES python${_CURRENT_VERSION_NO_DOTS} python${_CURRENT_VERSION}
    PATHS
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/libs
    # Avoid finding the .dll in the PATH.  We want the .lib.
    NO_SYSTEM_ENVIRONMENT_PATH
  )
  # Look for the static library in the Python config directory
  FIND_LIBRARY(PYTHON3_LIBRARY
    NAMES python${_CURRENT_VERSION_NO_DOTS} python${_CURRENT_VERSION}
    # Avoid finding the .dll in the PATH.  We want the .lib.
    NO_SYSTEM_ENVIRONMENT_PATH
    # This is where the static library is usually located
    PATH_SUFFIXES python${_CURRENT_VERSION}/config
  )

  # For backward compatibility, honour value of PYTHON3_INCLUDE_PATH, if
  # PYTHON3_INCLUDE_DIR is not set.
  IF(DEFINED PYTHON3_INCLUDE_PATH AND NOT DEFINED PYTHON3_INCLUDE_DIR)
    SET(PYTHON3_INCLUDE_DIR "${PYTHON3_INCLUDE_PATH}" CACHE PATH
      "Path to where Python.h is found for Python3" FORCE)
  ENDIF(DEFINED PYTHON3_INCLUDE_PATH AND NOT DEFINED PYTHON3_INCLUDE_DIR)

  SET(PYTHON3_FRAMEWORK_INCLUDES)
  IF(Python_FRAMEWORKS AND NOT PYTHON3_INCLUDE_DIR)
    FOREACH(dir ${Python_FRAMEWORKS})
      SET(PYTHON3_FRAMEWORK_INCLUDES ${PYTHON_FRAMEWORK_INCLUDES} ${PYTHON3_FRAMEWORK_INCLUDES}
        ${dir}/Versions/${_CURRENT_VERSION}/include/python${_CURRENT_VERSION})
    ENDFOREACH(dir)
  ENDIF(Python_FRAMEWORKS AND NOT PYTHON3_INCLUDE_DIR)

  FIND_PATH(PYTHON3_INCLUDE_DIR
    NAMES Python.h
    PATHS
      ${PYTHON_FRAMEWORK_INCLUDES}
      [HKEY_LOCAL_MACHINE\\SOFTWARE\\Python\\PythonCore\\${_CURRENT_VERSION}\\InstallPath]/include
    PATH_SUFFIXES
      /python${_CURRENT_VERSION}
  )
  MESSAGE(STATUS "Checked Python3 includes ${_CURRENT_VERSION} ${PYTHON3_INCLUDE_DIR} ${PYTHON3_FRAMEWORK_INCLUDES}")
  # For backward compatibility, set PYTHON_INCLUDE_PATH, but make it internal.
  SET(PYTHON3_INCLUDE_PATH "${PYTHON3_INCLUDE_DIR}" CACHE INTERNAL
    "Path to where Python.h is found for Python3 (deprecated)")

ENDFOREACH(_CURRENT_VERSION)


# STRING(REGEX REPLACE ".*/libpython([0-9 .]+).so" "\\1" PythonLibs_VERSION ${PYTHON_LIBRARY})
STRING(REGEX REPLACE ".*/libpython([0-9.]+[a-z]?)\\.so" "\\1" PythonLibs3_VERSION ${PYTHON3_LIBRARY})
MESSAGE(STATUS "Found PythonLibs version ${PythonLibs3_VERSION}")
MESSAGE(STATUS "Python includes are in  ${PYTHON3_INCLUDE_DIR}")


#
# Version checking
#
if(PythonLibs3_FIND_VERSION AND PythonLibs3_VERSION)
    if(PythonLibs3_FIND_VERSION_EXACT)
        if(NOT PythonLibs3_VERSION VERSION_EQUAL ${PythonLibs3_FIND_VERSION})
          MESSAGE(STATUS "Found PythonLibs3 ${PythonLibs3_VERSION} != ${PythonLibs3_FIND_VERSION} (Exact match required)")
          unset(PYTHONLIBS3_FOUND CACHE)
          RETURN()
        endif()
    else()
        # version is too low
        if(NOT PythonLibs3_VERSION VERSION_EQUAL ${PythonLibs3_FIND_VERSION} AND 
                NOT PythonLibs3_VERSION VERSION_GREATER ${PythonLibs3_FIND_VERSION})
          MESSAGE(STATUS "Found PythonLibs3 ${PythonLibs3_VERSION} < ${PythonLibs3_FIND_VERSION} (Minimum required).")
          unset(PYTHONLIBS3_FOUND CACHE)
          RETURN()    
        endif()
    endif()
endif()









MARK_AS_ADVANCED(
  PYTHON3_DEBUG_LIBRARY
  PYTHON3_LIBRARY
  PYTHON3_INCLUDE_DIR
)

# We use PYTHON_INCLUDE_DIR, PYTHON_LIBRARY and PYTHON_DEBUG_LIBRARY for the
# cache entries because they are meant to specify the location of a single
# library. We now set the variables listed by the documentation for this
# module.
SET(PYTHON3_INCLUDE_DIRS "${PYTHON3_INCLUDE_DIR}")
SET(PYTHON3_LIBRARIES "${PYTHON3_LIBRARY}")
SET(PYTHON3_DEBUG_LIBRARIES "${PYTHON3_DEBUG_LIBRARY}")

#INCLUDE(${CMAKE_CURRENT_LIST_DIR}/FindPackageHandleStandardArgs.cmake)
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PythonLibs3 DEFAULT_MSG PYTHON3_LIBRARIES PYTHON3_INCLUDE_DIRS)

